import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Driver {

	public void Menu() {
		Scanner scan = new Scanner(System.in);
		
		//This will handle the main menu
		//Tried to make sure I could prevent the maximum amount of wrong input.
		//It will only exit if if run one of the choices.
		boolean IsItWorking = true;
		
		while (IsItWorking) {
			System.out.println("Welcome to Database accidents. What would you like to Choose?");
			System.out.println("(1) Add accident to the Database.");
			System.out.println("(2) Find accident Info by ID");
			System.out.println("(3) Search by different types of range\n");

			while (!scan.hasNextInt()) {
				scan.next();
				System.out.println("Wrong input. Please try again");
			}
			int num = scan.nextInt();
			scan.nextLine();

			switch (num) {
			case 1:
				//
				IsItWorking = false;
				break;
			case 2:
				case2();
				IsItWorking = false;
				break;
			case 3:
				//
				case3();
				IsItWorking = false;
				break;
			default:
				System.out.println("Wrong Input. Try Again");
				IsItWorking = true;
				break;

			}
		}
		scan.close();
	}
	//this handles case2 which is search by ID. 
	//Once it gets the right information it will call searchByID
	public void case2() {

		boolean isItRunning = true;
		while (isItRunning) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Please insert an AID number: \n");

			while (!scan.hasNextInt()) {
				scan.next();
				System.out.println("I'm sorry you didn't input an number. Try Again");
			}
			int num = scan.nextInt();
			System.out.flush();
			searchByAID(num);
			isItRunning = false;
			scan.close();
		}

	}
	//case3 is handling the right methods calls to load the sub-menues. 
	//Once the info is returned from those methods calls. it will call searchByRanges() and pass it on the information
	//I was having an weird bug that the methods would stop running while gathering the data
	//I was able to circumvent this by passing the scanner as a parameter and by putting a bunch of scan.nextLine() after it gets the data
	//As this seems to flush the stream if other things were pressed.

	public void case3() {

		Scanner scanner = new Scanner(System.in);
		String[] dates = inputDate(scanner);
		double[] avgDmg = inputAvgDamages(scanner);
		double[] tDmg = inputTDamages(scanner);
		
		searchByRanges(dates[0], dates[1], avgDmg[0], avgDmg[1], tDmg[0], tDmg[1]);

		scanner.close();
	}
	//This function will acquire this info for starting date and ending date used by searchbyrange
	//The person has a choice to decline. If they do, defaultValues will be put into places.
	//It return a String[] with those values. [0] contains starting date and [1] contains ending dates
	public String[] inputDate(Scanner scan) {
		String[] dates = new String[2];
		String defaultBeginDate = "1800-01-30";
		String defaultEndDate = "2023-01-30";
		String dateRegex = "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])";

		String startingDate = "";
		String endingDate = "";
		boolean isItRunning = true;

		while (isItRunning) {
			System.out.println("Please input criterias that you would like to add. \n");

			System.out.println("Would you like to add range of dates? (y)es or (n)o? \n");

			String line = scan.next();
			scan.nextLine();
			char option = line.charAt(0);
			option = Character.toLowerCase(option);

			switch (option) {
			case 'y':
				System.out.println("Please add the starting date in the formation YYYY-MM-DD: \n");
				startingDate = scan.next();
				scan.nextLine();
				if (!startingDate.matches(dateRegex)) {
					System.out.println("Sorry, wrong format! Try Again");
					isItRunning = true;
					break;
				}

				System.out.println("Please add the end date in the formation YYYY-MM-DD: \n");
				endingDate = scan.next();
				scan.nextLine();
				if (!endingDate.matches(dateRegex)) {
					System.out.println("Sorry, wrong format! Try Again");
					isItRunning = true;
					break;
				}
				isItRunning = false;
				break;
			case 'n':
				startingDate = defaultBeginDate;
				endingDate = defaultEndDate;
				isItRunning = false;
				break;
			default:
				System.out.println("Sorry, wrong format or Answer! Try Again");
				isItRunning = true;
				break;

			}

		}

		dates[0] = startingDate;
		dates[1] = endingDate;

		return dates;
	}
	//This function will acquire this info for starting average for damages and ending average of damages used by searchbyrange
	//The person has a choice to decline. If they do, defaultValues will be put into places.
	//It return a String[] with those values. [0] contains starting average and [1] contains ending average
	public double[] inputAvgDamages(Scanner scan) {
		double[] avgDamages = new double[2];
		double defaultBeginDamages = 0.0;
		double defaultEndDamages = 999999999.0;
		String line;
		char option;
		boolean isItRunning = true;

		while (isItRunning) {

			System.out.println("Would you like to add averages of damages? (y)es or (n)o? \n");
			line = scan.next();
			scan.nextLine();
			option = line.charAt(0);
			option = Character.toLowerCase(option);

			switch (option) {
			case 'y':
				System.out.println("Please add the initial damages: format 0000.00");
				while (!scan.hasNextDouble()) {
					scan.nextLine();
					System.out.println("I'm sorry You didn't input a number. Please try again.");
				}
				avgDamages[0] = scan.nextDouble();
				scan.nextLine();
				System.out.println("Please add the ending damages: format 0000.00");
				while (!scan.hasNextDouble()) {
					scan.nextLine();
					System.out.println("I'm sorry You didn't input a number. Please try again.");
				}
				avgDamages[1] = scan.nextDouble();
				scan.nextLine();
				isItRunning = false;
				break;
			case 'n':
				avgDamages[0] = defaultBeginDamages;
				avgDamages[1] = defaultEndDamages;
				isItRunning = false;
				break;
			default:
				isItRunning = true;
				System.out.println("I'm sorry your input is wrong. Please try again");

			}
		}

		return avgDamages;
	}

	//This function will acquire this info for starting total damage and ending total damage used by searchbyrange
	//The person has a choice to decline. If they do, defaultValues will be put into places.
	//It return a String[] with those values. [0] contains starting average and [1] contains ending average
	public double[] inputTDamages(Scanner scan) {
		double[] tDamages = new double[2];
		double defaultBeginDamages = 0.0;
		double defaultEndDamages = 999999999.0;
//		Scanner scan = new Scanner(System.in);
		String line;
		char option;
		boolean isItRunning = true;

		while (isItRunning) {

			System.out.println("Would you like to add range for Total amount of damages? (y)es or (n)o? \n");
			line = scan.next();
			scan.nextLine();
			option = line.charAt(0);
			option = Character.toLowerCase(option);

			switch (option) {
			case 'y':
				System.out.println("Please add the initial total damages: format 0000.00");
				while (!scan.hasNextDouble()) {
					scan.nextLine();
					System.out.println("I'm sorry You didn't input a number. Please try again.");
				}
				tDamages[0] = scan.nextDouble();
				scan.nextLine();
				System.out.println("Please add the ending total damages: format 0000.00");
				while (!scan.hasNextDouble()) {
					scan.nextLine();
					System.out.println("I'm sorry You didn't input a number. Please try again.");
				}
				tDamages[1] = scan.nextDouble();
				scan.nextLine();
				isItRunning = false;
				break;
			case 'n':
				tDamages[0] = defaultBeginDamages;
				tDamages[1] = defaultEndDamages;
				isItRunning = false;
				break;
			default:
				isItRunning = true;
				System.out.println("I'm sorry, your input is wrong. Please try again");

			}
		}

		return tDamages;
	}
	//case1() handles the getting the information needed to add the accident into the accident and involvement table
	//it is supposed to handle invalid input to the best of my abilities.
	//case one is huge. I should have divided, but it is working.
	public void case1() {
		Scanner scan = new Scanner(System.in);
		boolean isItRunning = true;
		boolean isItRunning2 = true;
		boolean isItRunning3 = true;
		String startingDate = "";
		String city = "";
		String state = "";
		String vin = "";
		String damage = "";
		String driverSSN = "";
		int carAmt = 0;
		String dateRegex = "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])";
		String ssnRegex = "^(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}$";
		
		//this first while loop makes sure that the person put the date, city, and state
		//It checks the format of year in order to be YYYY-MM-DD
		//it will only get out of the loop once it gets all info
		while (isItRunning) {
			System.out.println("Please fill out the following information about the accident.");
			System.out.println("Date that the accident happened. format YYYY-MM-DD");
			startingDate = scan.next();
			scan.nextLine();
			if (!startingDate.matches(dateRegex)) {
				System.out.println("Sorry, wrong format! Try Again");
				isItRunning = true;
			} else {
				isItRunning = false;
			}
		}
		System.out.println("City: ");
		while (city.isEmpty()) {
			city = scan.next();
			if (city.isEmpty()) {
				System.out.println("Sorry, You have to put a City. Try Again");
			}
		}
		scan.nextLine();

		System.out.println("State: ");
		while (state.isEmpty() || (state.length() > 2 || state.length() < 2)) {
			state = scan.next().toUpperCase();
			if (state.isEmpty() || (state.length() > 2 || state.length() < 2)) {
				System.out.println("Sorry, You have to put a City. Try Again");
			}
		}
		scan.nextLine();

		System.out.println("Accident Date: " + startingDate + "\nCity: " + city + "\nState: " + state);

		// this function will add the info into the accident table.
		AddAccident(startingDate, city, state);
		
		//This wild loop handles getting how many cars were in the accident.
		//it will not accept anything less than 1.
		
		while (isItRunning2) {
			System.out
					.println("How many cars were involved in the accident that you would like to fill out information");
			while (!scan.hasNextInt()) {
				scan.nextLine();
				System.out.println("I'm sorry You didn't input a number. Please try again.");
			}
			carAmt = scan.nextInt();
			scan.nextLine();
			if (carAmt < 1) {
				isItRunning = true;
				System.out.println("I'm sorry, amount of car should be 1 or greater");
			} else {
				isItRunning2 = false;
			}
		}
		
		System.out.println("Number of car involved in the accident: " + carAmt);
		//this for loop will handle getting the info of each car that was involved in the accident
		for (int i = 0; i < carAmt; i++) {
			while (isItRunning3) {
				System.out.println("Please type the the vin Number for Car number " + (i + 1) + ": ");
				//get the vin
				while (vin.isEmpty() || vin.length() < 5) {
					vin = scan.next().toUpperCase();
					if (state.isEmpty() || vin.length() < 5) {
						System.out.println("Sorry, You didn't put a vin number or it was too small. Try Again");
					}
				}
				scan.nextLine();

				// gets the damage
				System.out.println("What was the damage in car number " + (i + 1) + ": ");
				while (!scan.hasNextDouble()) {
					scan.nextLine();
					System.out.println("I'm sorry You didn't input a number. Please try again.");
				}
				double temp = scan.nextDouble();
				scan.nextLine();
				damage = Double.toString(temp);

				// gets the driver_ssn
				System.out.println(
						"Please type the SSN for the driver of car number " + (i + 1) + ": Format XXX-XX-XXXX");
				while (driverSSN.isEmpty() || !driverSSN.matches(ssnRegex)) {
					driverSSN = scan.next().toUpperCase();
					if (state.isEmpty() || !driverSSN.matches(ssnRegex)) {
						System.out.println("Sorry, You didn't put correct SSN or it was the wrong format. Try Again");
					}
				}
				scan.nextLine();

				System.out.println("Information for the car number " + (i + 1) + " VIN: " + vin + " Damages: " + damage
						+ " driver SSN: " + driverSSN);
				isItRunning3 = false; // change this is for testing
			}
			if (i < carAmt) {
				isItRunning3 = true;
				//adds to the Involvement table
				AddInvolvement(vin, damage, driverSSN);
				//reset the data for the next round of car
				vin = "";
				damage = "";
				driverSSN = "";
			} else {
				isItRunning3 = false;
			}
		}
		scan.close();
	}
	//search by AID, creates a query to search for accidents based by AID
	public void searchByAID(int AID) {
		try {
			//this creates a connection
			String urlString = "jdbc:sqlite:" + "MyLibs/autosDB.sqlite";
			Class.forName("org.sqlite.JDBC");
			Connection con = DriverManager.getConnection(urlString);

			Statement stmt;
			//creating a statement
			stmt = con.createStatement();

			String statement = "SELECT accident_date, city, state, vin, damages, driver_ssn "
					+ "FROM accidents, involvements " + "WHERE accidents.aid = '" + AID + "' "
					+ "AND involvements.aid = '" + AID + "' ";
			
			//executing the statement and getting the result.
			ResultSet rs;
			rs = stmt.executeQuery(statement);
			//printing the header of the table
			String str1 = String.format("|%14s|%14s|%14s|%14s|%14s|%14s|", "accident_date", "city", "state", "vin",
					"damages", "driver_ssn");

			System.out.println(str1);
			
			//this will print each row for the results
			while (rs.next()) {
				double damages1 = Double.parseDouble(rs.getString(5));
				String str2 = String.format("|%14s|%14s|%14s|%14s|%14.2f|%14s|", rs.getString(1), rs.getString(2),
						rs.getString(3), rs.getString(4), damages1, rs.getString(6));
				System.out.println(str2);

			}

			rs.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	//this function will create a query to search by ranges based on the info received on the parameters
	//it takes 6 parameters each representing of the the requirements beginDate and endDate for the range in date
	//beginDamages and endDamages for the range of averages that you want to search
	//beginTDamages and endTDamages for range of total damages you are looking for
	public void searchByRanges(String beginDate, String endDate, double beginDamages, double endDamages,
			double beginTDamages, double endTDamages) {
		try {
			//creates connection
			String urlString = "jdbc:sqlite:" + "MyLibs/autosDB.sqlite";
			Class.forName("org.sqlite.JDBC");
			Connection con = DriverManager.getConnection(urlString);

			Statement stmt;
			//create a statement
			stmt = con.createStatement();
			//statement being build based on the info received from the parameters
			String statement = "SELECT accidents.aid, accident_date, city, state, avg(damages), sum(damages) FROM accidents INNER JOIN involvements ON accidents.aid = involvements.aid "
					+ "GROUP BY accidents.aid " + "HAVING accident_date BETWEEN " + " '" + beginDate + "' AND '"
					+ endDate + "' AND avg(damages) BETWEEN " + beginDamages + " AND " + endDamages
					+ " AND sum(damages) BETWEEN " + beginTDamages + " AND " + endTDamages + " ORDER BY accidents.aid ";
			
			ResultSet rs;
			//execute the query and gets the results
			rs = stmt.executeQuery(statement);
			
			String str1 = String.format("|%14s|%14s|%14s|%14s|%14s|%14s|", "accident.aid", "accident_date", "city", "state",
					"avg(Damages)", "sum(Damages)");
			//prints on the header
			System.out.println(str1);
			
			//while loop prints each line
			while (rs.next()) {
				double damages1 = Double.parseDouble(rs.getString(5));
				String str2 = String.format("|%14s|%14s|%14s|%14s|%14.2f|%14s|", rs.getString(1), rs.getString(2),
						rs.getString(3), rs.getString(4), damages1, rs.getString(6));
				System.out.println(str2);

			}

			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//AddAccident will add a new accident into the table of accident by using the parameters
	public void AddAccident(String accidentDate, String city, String state) {

		try {
			String urlString = "jdbc:sqlite:" + "MyLibs/autosDB.sqlite";
			Class.forName("org.sqlite.JDBC");
			Connection con = DriverManager.getConnection(urlString);

			Statement stmt;

			stmt = con.createStatement();
			//first I create a query to get the max number in accidents.id

			String statement = "SELECT max(accidents.aid) FROM accidents";
			//execute the query and gets the results back.
			ResultSet rs;
			rs = stmt.executeQuery(statement);
			
			int count = rs.getInt(1);
			//then I add one to it, this will allow it to be unique number
			count++;
			System.out.println(count);
			//create a string for the statement
			String insertRow = "INSERT INTO accidents (aid, accident_date, city, state)\n" + "VALUES (?,?,?,?)";
			//this prepare the statement to receive the attributes.
			PreparedStatement pstmt = con.prepareStatement(insertRow);
			pstmt.setInt(1, count);
			pstmt.setString(2, accidentDate);
			pstmt.setString(3, city);
			pstmt.setString(4, state);
			pstmt.executeUpdate();

			System.out.println("Table Updated, Row added");

			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	//AddInvolvement will add the cars information into involvements table.
	public void AddInvolvement(String vin, String damages, String driver_ssn) {

		try {
			//gets the connection
			String urlString = "jdbc:sqlite:" + "MyLibs/autosDB.sqlite";
			Class.forName("org.sqlite.JDBC");
			Connection con = DriverManager.getConnection(urlString);

			Statement stmt;
			//create a statement
			stmt = con.createStatement();

			String statement = "SELECT max(accidents.aid) FROM accidents";
			//get the results for the max aid (reminder) this should be the same number that was recently added in addAccident.
			//this functions gets called multiple times by case1, so it will use same accident aid for the most recent added accident.
			ResultSet rs;
			rs = stmt.executeQuery(statement);
			int count = rs.getInt(1);
			System.out.println(count);
			
			//turns damage string into double, so it can be added to the tabel.
			double damages1 = Double.parseDouble(damages);
			String insertRow = "INSERT INTO involvements (aid, vin, damages, driver_ssn)\n" + "VALUES (?,?,?,?)";

			PreparedStatement pstmt = con.prepareStatement(insertRow);
			pstmt.setInt(1, count);
			pstmt.setString(2, vin);
			pstmt.setDouble(3, damages1);
			pstmt.setString(4, driver_ssn);
			pstmt.executeUpdate();
			System.out.println("Table Updated, Row added");

			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
